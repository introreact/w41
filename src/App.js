import React from 'react';
import './App.css';
import Interval from './components/Interval';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Interval />
      </header>
    </div>
  );
}

export default App;
