import React from "react";
import { useState } from "react";

const Interval = () => {
    const [time, setTime] = useState([]);

    const date = new Date();
    const hours = date.getHours();
    let hoursTime = `${hours}`;
    const minutes = date.getMinutes();
    let minutesTime = `${minutes}`;
    const seconds = date.getSeconds();
    let secondsTime = `${seconds}`;
    const milliseconds = date.getMilliseconds();
    let millisecondsTime = `${milliseconds}`;

    if(hours < 10) {
        hoursTime = `0${hours}`
    }
    if (minutes < 10) {
        minutesTime = `0${minutes}`
    }
    if (seconds < 10) {
        secondsTime = `0${seconds}`
    }
    if (milliseconds < 10) {
        millisecondsTime = `00${milliseconds}`
    } else if (milliseconds < 100) {
        millisecondsTime = `0${milliseconds}`
    }

    let timeNow = `${hoursTime}:${minutesTime}:${secondsTime}-${millisecondsTime}`;


    const handleClick = () => {
        setTime(prevTime => {
            return [...prevTime, timeNow]
        });
    }

    return (
        <div>
            <h1>Interval</h1>
            <p>w41</p>
            <button className="btn" value={timeNow} onClick={handleClick}>Interval time</button>
            {time.map((a) => {
                return (
                <div key={a}>{a}</div>
                )
            })
            }
        </div>
    )
}

export default Interval